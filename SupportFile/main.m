//
//  main.m
//  Note
//
//  Created by Denis Lyakhovich on 29.03.18.
//  Copyright © 2018 Denis Lyakhovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
